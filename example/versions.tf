terraform {
  backend "s3" {
    profile = "terraform_user"
    key     = "myproject-llqzh7ok/terraform.tfstate"
  }
  required_version = ">= 0.15"
}
