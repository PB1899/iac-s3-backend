## Sample configuraton created running the script:
```bash
$ ./setup.sh -n myproject -p terraform_user -r eu-west-1
```
After this I applied the configuration:

![TF-apply-output](Screenshot_TF_apply.png)

#### Terraform state file on S3:

![S3-console](Screenshot_S3_console.png)

####  DynamoDB table for locking

![DynamoDB-console](Screenshot_DynamoDB_console.png)

*nb. The folder represents the state before cleanup and is stored here for demonstration purposes, it has now been cleaned up using the destroy.sh script.*
![TF-destroy-output](Screenshot_TF_destroy.png)