
variable "namespace" {
  description = "the name of your project"
  type        = string
  default     = "myproject"
}

variable "aws_profile" {
  description = "your AWS profile"
  type        = string
}

variable "aws_region" {
  description = "AWS region to deploy to"
  type        = string
}
