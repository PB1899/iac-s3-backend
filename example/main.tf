resource "null_resource" "example" {
  triggers = {
    always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo Your configuration runs with S3 backend remote state!"
  }
}
