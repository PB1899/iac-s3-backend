#!/bin/bash

# script to remove the remote backend, only run
# AFTER YOU DESTROYED YOUR CONFIGURATION!

echo " Have you destroyed your configuration yet? (yes/no) "
read CHECK

if [[ $CHECK == "yes" ]]; then
  cd s3-backend
  export BUCKETNAME=$(terraform output -raw s3_bucket)
  sed -i 's/prevent_destroy = true/prevent_destroy = false/' main.tf

  echo -e "\nFormatting the Terraform files...\n"

  terraform fmt

  echo -e "\nRemoving the object versions from S3...\n"

  aws s3api delete-objects \
    --bucket $BUCKETNAME \
    --delete "$(aws s3api list-object-versions \
    --bucket "$BUCKETNAME" \
    --output=json \
    --query='{Objects: Versions[].{Key:Key,VersionId:VersionId}}')"

  terraform destroy
else
  echo "---------------------------------------------"
  echo "You need to destroy your configuration first!"
  echo "---------------------------------------------"
fi
