## Bash script to create your S3 backend for Terraform projects

Prerequisites:
- [Terraform](https://www.terraform.io/) installed (>= 0.15.0)
- AWS account with a profile that can create resources
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) installed

The script will create the following directory/files:

```
s3-backend/        
          main.tf
          outputs.tf
          variables.tf
          versions.tf
          terraform.tfvars
main.tf
versions.tf
backend.hcl
```
**! It's a good idea to add backend.hcl to your .gitignore !**

The script will create the following AWS resources(may incure costs if you're not on the free tier):
- S3 bucket to store the Terraform state file
- DynamoDB table to store the lock

#### Possible ways to improve the script

- **IAM role**  
the script presumes that your profile uses an existing IAM role, should you wish to create one specifically for this purpose, please feel free to do so.
For an example, please see the [code](https://github.com/terraform-in-action/manning-code/tree/master/chapter6) from 'Terraform in action' by [Scott Winkler](https://github.com/scottwinkler).
- **AWS regions declared via environment variable**  
the script handles the AWS region explicitly declared as an option for setup.sh or fetches information about the default region of the given profile used in the AWS config (~/.aws/). In case another region was declared using the *AWS_DEFAULT_REGION* environment variable, you'll have to use
  ```bash
  $ aws configure list | grep region | awk '{print $2}'
  ```
  Please see this [Stack Overflow question](https://stackoverflow.com/questions/31331788/using-aws-cli-what-is-best-way-to-determine-the-current-region#41977894)

### setup.sh

Run the script from your command line as shown in the example:
```bash
$ ./setup.sh --namespace "myproject"
```
You can refer to more info running 
```bash
$ ./setup.sh --help
```

### destroy.sh

The backend configuration prevents from accidentally deleting the S3 bucket, and the lifecyle section need to be changed accordingly to allow for deleting.
S3 won't allow you to remove a non-empty bucket, so it's necessary to delete all versions and object before it can be destroyed. 
The script will clean up and destroy the S3 bucket and the DynamoDB table, only run **AFTER** having destroyed your main Terraform configuration!
```bash
$ ./destroy.sh
```
as a safety measure, the script will prompt you for confirmation, and prompt once more to approve the ```terraform destroy``` command.

### Sample configuration

A sample configuration is saved in the *example* folder with screenshots