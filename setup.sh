#!/bin/bash

function usage() {
    cat <<USAGE

    Usage: $0 -n namespace [--configure-aws] [--profile aws_profile] [--region aws_region]

    Options:
        -n, --namespace:      the name of your project, only lowercase alphanumeric characters and hyphen allowed
                              (required)
        -p, --profile:        your AWS profile
                              (optional, choose if not the default is used)
        -r, --region:         AWS region to use
                              (optional, choose if not the default is used or no region is set in ~/.aws/config)
        -c, --configure-aws:   provide your AWS credentials on the command line
                              (optional, choose if not set in ~/.aws/credentials)
USAGE
    exit 1
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

CONFIGURE_AWS="false"
NAMESPACE=
PROFILE="default"
REGION=$(aws configure get region)

while [ "$1" != "" ]; do
    case $1 in
    -c | --configure-aws)
        CONFIGURE_AWS=true
        ;;
    -n | --namespace)
        shift
        NAMESPACE=$1
        ;;
    -p | --profile)
        shift
        PROFILE=$1
        REGION=$(aws configure get region --profile "$PROFILE")
        ;;
    -r | --region)
        shift
        REGION=$1
        ;;
    -h | --help)
        usage
        ;;
    *)
        usage
        exit 1
        ;;
    esac
    shift
done

if [[ $NAMESPACE == "" ]]; then
  echo "the namespace can't be an empty string";
  usage
  exit 1;
fi

if [[ $PROFILE == "" ]]; then
  echo "the AWS profile can't be an empty string";
  usage
  exit 1;
fi

if [[ $CONFIGURE_AWS == "true" ]]; then
  echo "AWS_ACCESS_KEY_ID:"
  read ACCESS_KEY
  echo "AWS_SECRET_ACCESS_KEY:"
  read SECRET_KEY

  export AWS_ACCESS_KEY_ID=$ACCES_KEY
  export AWS_SECRET_ACCESS_KEY=$SECRET_KEY
fi

if [ -d s3-backend ]; then
  echo "You already have a directory called s3-backend"
else
  mkdir s3-backend
fi

cat > s3-backend/variables.tf << VARS

variable "namespace" {
description = "the name of your project"
type = string
default = "$NAMESPACE"
}

variable "aws_profile" {
description = "your AWS profile"
type = string
}

variable "aws_region" {
description = "AWS region to deploy to"
type = string
}
VARS

cat > s3-backend/terraform.tfvars << TFVARS
aws_profile = "$PROFILE"
aws_region = "$REGION"
TFVARS

cat > s3-backend/versions.tf << VERSIONS

terraform {
  required_version = ">= 0.15"
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.28"
      }
      random = {
          source = "hashicorp/random"
          version = "~> 3.0"
      }
  }
}
VERSIONS

cat > s3-backend/main.tf << BACKEND
provider "aws" {
  region = var.aws_region
  profile = var.aws_profile
}

resource "random_string" "rand" {
  length  = 8
  special = false
  upper   = false
}

locals {
  namespace_string = substr(join("-", [var.namespace, random_string.rand.result]), 0, 24)
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "\${local.namespace_string}-bucket"

  # prevent accidental deletion
  lifecycle {
    prevent_destroy = true
  }

  # enable versioning
  versioning {
    enabled = true
  }

  #server-side encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "\${local.namespace_string}-table"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
BACKEND

cat > s3-backend/outputs.tf << OUTPUTS
output "s3_bucket_arn" {
  value       = aws_s3_bucket.terraform_state.arn
  description = "The ARN of the S3 bucket"
}

output "s3_bucket" {
  value = aws_s3_bucket.terraform_state.bucket
  description = "the S3 bucket"
}

output "dynamodb_table_name" {
  value       = aws_dynamodb_table.terraform_locks.name
  description = "The name of the DynamoDB table"
}

output "namespace_string" {
  value = local.namespace_string
}
OUTPUTS

echo -e "\nFormatting the Terraform files...\n"
terraform fmt -recursive

cd s3-backend
terraform init
terraform validate
terraform apply --auto-approve

export BUCKETNAME=$(terraform output -raw s3_bucket)
export TABLENAME=$(terraform output -raw dynamodb_table_name)
export KEYNAME=$(terraform output -raw namespace_string)

cd ..

cat > versions.tf << VERSIONS
terraform {
  backend "s3" {
    profile        = "$PROFILE"
    key            = "$KEYNAME/terraform.tfstate"
  }
  required_version = ">= 0.15"
}
VERSIONS

cat > main.tf << MAIN
resource "null_resource" "example" {
  triggers = {
    always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo Your configuration runs with S3 backend remote state!"
  }
}
MAIN

cat > backend.hcl << BACKENDCONFIG

bucket         = "$BUCKETNAME"
region         = "$REGION"
dynamodb_table = "$TABLENAME"
encrypt        = true
BACKENDCONFIG

echo -e "\nFormatting the Terraform files...\n"

terraform fmt
terraform init -backend-config=backend.hcl
terraform validate